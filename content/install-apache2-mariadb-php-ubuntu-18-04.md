---
title: Install Apache2, MariaDB dan PHP di Ubuntu 18.04
date: 2020-07-12
tags: ["ubuntu", "apache2", "mariadb", "php"]
categories: Linux Admin Tutorials
---

Berikut tutorials install Apache2, MariaDB dan PHP di Ubuntu 18.04.

<!--more-->

## Update package manager

Sebelum install Apache2, MariaDB dan PHP. Update terlebih dahulu package manager Ubuntu 18.04.

```
sudo apt-get update -y
sudo apt-get upgrade -y
```

## Install Apache2

Install dan jalankan service Apache2.

```
sudo apt-get install apache2 -y
sudo systemctl start apache2
sudo systemctl enable apache2
```

Test service Apache2 dengan mengakses url `http://localhost/` pada browser. Jika berhasil maka akan tampil page default Apache2.

## Install MariaDB

Install dan jalankan service MariaDB server.

```
sudo apt-get install mariadb-server -y
sudo systemctl start mariadb
sudo systemctl enable mariadb
```

Setup password root MariaDB server.

```
sudo mysql_secure_installation
```

## Install PHP

Install PHP beserta extensions yang diperlukan.

```
sudo apt-get install php php-{bcmath,bz2,intl,gd,mbstring,mysql,zip,fpm} -y
```

Restart service Apache2 untuk mengaktifkan PHP yang sudah diinstall tersebut.

```
sudo systemctl restart apache2
```

## Congratulations

Install Apache2, MariaDB dan PHP di Ubuntu 18.04 berhasil.