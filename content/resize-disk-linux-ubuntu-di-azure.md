---
title: Resize disk Linux Ubuntu di Azure
date: 2020-07-23
tags: ["ubuntu"]
categories: Linux Admin Tutorials
---

Default image Linux di Azure memiliki ukuran 30GB untuk osDisk dan itu sesuatu yang kedepannya akan diresize.

Resize partisi di Azure sangat simple, cukup mengikuti step-step di bawah ini:

- Stop dan deallocate VM terlebih dahulu di portal Azure;
- Pilih disk yang akan diresize pada menu `Disk`;
- Pilih menu `Configuration` dan tambahkan size terbaru dengan ukuran GiB;
- Setelah disimpan, nyalakan kembali VM tersebut.

<!--more-->

> NOTE: Sebelum resize disk, alangkah baiknya backup terlebih dahulu atau snapshot disk ketika VM deallocating. Hal ini untuk antisipasi kegagalan dalam resize disk.

System Linux Ubuntu akan otomatis resize ketika boot. Jadi kita tidak perlu manual resize partisinya.
