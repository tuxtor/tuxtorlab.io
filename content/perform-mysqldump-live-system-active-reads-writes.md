---
title: Perform mysqldump live system active reads writes
date: 2020-07-24
tags: ["mysql", "mariadb", "backup"]
categories: Linux Admin Tutorials
---

Cara backup MySQL dan MariaDB dengan menggunakan mysqldump dengan aman, berikut caranya.

**InnoDB**

```
mysqldump -uUSERNAME -pPASSWORD --single-transaction --routines --triggers --events DBNAME > DBNAME.sql
```

<!--more-->

**MyISAM**

```
mysql -uUSERNAME -pPASSWORD -Ae"FLUSH TABLES WITH READ LOCK; SELECT SLEEP(86400)" &
sleep 5
mysql -uUSERNAME -pPASSWORD -ANe"SHOW PROCESSLIST" | grep "SELECT SLEEP(86400)" > /tmp/proclist.txt
SLEEP_ID=`cat /tmp/proclist.txt | awk '{print $1}'`
echo "KILL ${SLEEP_ID};" > /tmp/kill_sleep.sql
mysqldump -uUSERNAME -pPASSWORD --single-transaction --routines --triggers --events DBNAME > DBNAME.sql
mysql -uUSERNAME -pPASSWORD -A < /tmp/kill_sleep.sql
```
