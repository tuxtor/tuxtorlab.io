---
title: Reset password root MySQL di Linux Debian
date: 2020-07-21
tags: ["mysql", "debian"]
categories: Linux Admin Tutorials
---

Jika lupa password root MySQL, jangan panik, kita bisa reset password root MySQL di Linux Debian dengan langkah berikut.

Stop service MySQL terlebih dahulu:

```
service mysql stop
```

Grant password-less access:

```
mysqld_safe --skip-grant-tables &
```

<!--more-->

Login ke MySQL dengan menggunakan user **root** tanpa password:

```
mysql -u root mysql
```

Create password baru untuk user root:

```
UPDATE user SET password=PASSWORD('PASSWORD_BARU') WHERE user='root';
FLUSH PRIVILEGES;
```

Jalankan kembali service MySQL:

```
service mysql restart
```

Check apakah reset password root MySQL berhasil, caranya dengan login menggunakan password baru:

```
mysql -u root -p
```
