---
title: Resize disk Linux Centos 7 di Azure
date: 2020-07-17
tags: ["centos"]
categories: Linux Admin Tutorials
---

Default image Linux di Azure memiliki ukuran 30GB untuk osDisk dan itu sesuatu yang kedepannya akan diresize.

Resize partisi di Azure sangat simple, cukup mengikuti step-step di bawah ini:

- Stop dan deallocate VM terlebih dahulu di portal Azure;
- Pilih disk yang akan diresize pada menu `Disk`;
- Pilih menu `Configuration` dan tambahkan size terbaru dengan ukuran GiB;
- Setelah disimpan, nyalakan kembali VM tersebut.

<!--more-->

> NOTE: Sebelum resize disk, alangkah baiknya backup terlebih dahulu atau snapshot disk ketika VM deallocating. Hal ini untuk antisipasi kegagalan dalam resize disk.

Step di atas hanya resize disk, partisi dalam system Linux Centos 7 masih menggunakan size sebelumnya. Untuk itu berikut caranya resize partisi di Linux Centos 7.

- Login ke VM dengan menggunakan SSH.
- Check partisi yang akan diresize: \
  `df -lh`
- Dalam contoh ini partisi **/dev/sda2** yang akan diresize dengan menggunakan command fdisk: \
  `fdisk /dev/sda` \
  ketik: `p` (untuk menampilkan partisi, dalam hal ini tampil partisi **/dev/sda1** dan **/dev/sda2**, partisi 1 dan 2 \
  ketik: `d` kemudian `2` (untuk menghapus partisi 2) \
  ketik: `n` kemudian `p` kemudian `2` (untuk membuat ulang partisi 2) \
  ketik: `w` (untuk menyimpan) \
  ketik: `q` (untuk keluar dari fdisk)
- Reboot system agar partisi terupdate
- Setelah proses reboot, selanjutnya step terakhir proses resize partisi: \
  `xfs_growfs /dev/sda2`
- Check kembali partisi tersebut apakah sudah menggunakan size terbarunya: \
  `df -lh`
