---
title: Bash script mysqldump backup all mysql databases
date: 2020-07-15
tags: ["mysql", "backup"]
categories: Linux Admin Tutorials
---

Mysqldump merupakan tools untuk mengexport atau membackup mysql database. Berikut bash script untuk membackup mysql database:

<!--more-->

```
#!/bin/bash

# Folder backup
DEST=/backupdb
CURRDATE=$(date +"%F")

# Mysql credentials
HOSTNAME="localhost"
USER="usermysql"
PASS="passwordmysql"

DATABASES=$(mysql -h $HOSTNAME -u $USER -p$PASS -e "SHOW DATABASES;" | tr -d "| " | grep -v Database)

[ ! -d $DEST/$CURRDATE ] && mkdir -p $DEST/$CURRDATE

for db in $DATABASES; do
  FILE="${DEST}/${CURRDATE}/$db.sql.gz"
  mysqldump --single-transaction --routines --triggers --events --quick -h $HOSTNAME -u $USER -p$PASS -B $db | gzip > "$FILE"
  
  # Delete all backups older than 7 days
  find $DEST -name '*.sql.gz' -type f -mtime +7 -exec rm -f {} \;
done
```

Save bash script tersebut dengan nama `mysqlbackup.sh` dan buat cronjob untuk menjalankan bash script tersebut setiap hari.

```
0 1 * * * ~/./mysqlbackup.sh
```