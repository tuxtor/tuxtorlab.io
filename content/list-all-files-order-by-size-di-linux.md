---
title: List All Files Order by Size di Linux
date: 2020-07-18
tags: ["centos", "ubuntu"]
categories: Linux Admin Tutorials
---

Untuk menampilkan semua file di Linux serta diurutkan berdasarkan size, berikut caranya dengan menggunakan command **ls**:

```
ls -laSh /namadirektori/
```

<!--more-->

Option **-S** untuk menyortir file berdasarkan size descending (dari besar ke kecil). Untuk menyortir file berdasarkan size ascending berikut caranya:

```
ls -laShr /namadirektori/
```

Dengan menambahkan option **r** akan menyortir file berdasarkan size ascending (dari kecil ke besar).
