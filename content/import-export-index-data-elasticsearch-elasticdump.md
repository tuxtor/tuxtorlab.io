---
title: Import dan Export index data Elasticsearch menggunakan Elasticdump
date: 2020-07-14
tags: ["elasticsearch"]
categories: Linux Admin Tutorials
---

Elasticdump merupakan tools untuk import dan export index elasticsearch. Elasticdump dapat digunakan untuk melakukan backup data index elasticsearch ke dalam file atau migrasi index elasticsearch ke server elasticsearch lainnya.

<!--more-->

## Install Elasticdump

Agar dapat menggunakan tools elasticdump, diperlukan nodejs versi 10 atau versi terbaru. Berikut cara install elasticdump:

```
wget -q -O elasticsearch-dump.zip https://github.com/elasticsearch-dump/elasticsearch-dump/archive/master.zip
unzip elasticsearch-dump.zip
cd elasticsearch-dump-master
npm install
```

## Export data index ke dalam file json

```
cd elasticsearch-dump-master/bin

./elasticdump \
  --input=http://localhost:9200/es_index \
  --output=es_index.json \
  --bulk=true
```

## Import data index ke elasticsearch

```
cd elasticsearch-dump-master/bin

./elasticdump \
  --input=es_index.json \
  --output=http://localhost:9200/es_index \
  --bulk=true
```

## Congratulations

Dengan mengikuti command line di atas, Anda dapat membackup dan merestore data index elasticsearch.
