---
title: Kubectl delete evicted Pods
date: 2020-07-16
tags: ["kubernetes"]
categories: DevOps Tutorials
---

Berikut cara untuk menghapus semua Pods yang statusnya `Evicted`:

```
kubectl get pods | grep Evicted | awk '{print $1}' | xargs kubectl delete pod
```

<!--more-->
