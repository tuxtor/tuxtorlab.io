---
title: Reset password root MariaDB di Linux Ubuntu
date: 2020-07-22
tags: ["mariadb", "ubuntu"]
categories: Linux Admin Tutorials
---

Jika lupa password root MariaDB, jangan panik, kita bisa reset password root MariaDB di Linux Ubuntu dengan langkah berikut.

Stop service MariaDB terlebih dahulu:

```
service mariadb stop
```

Grant password-less access:

```
mysqld_safe --skip-grant-tables &
```

<!--more-->

Login ke MariaDB dengan menggunakan user **root** tanpa password:

```
mysql -u root mysql
```

Create password baru untuk user root:

```
UPDATE user SET password=PASSWORD('PASSWORD_BARU') WHERE user='root';
FLUSH PRIVILEGES;
```

Jalankan kembali service MariaDB:

```
service mariadb restart
```

Check apakah reset password root MariaDB berhasil, caranya dengan login menggunakan password baru:

```
mysql -u root -p
```
