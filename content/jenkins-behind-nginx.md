---
title: Jenkins Behind Nginx
date: 2020-04-07
tags: ["jenkins"]
categories: DevOps Tutorials
---

Dalam situasi di mana Anda sudah memiliki web server Nginx, Anda mungkin ingin memanfaatkannya untuk menjalankan Jenkins, sehingga Anda dapat binding Jenkins ke bagian website yang lebih besar yang mungkin Anda miliki. Dokumen ini membahas beberapa hal untuk melakukan hal tersebut.

<!--more-->

Ketika ada request untuk URL tertentu, Nginx akan menjadi proxy dan meneruskan request tersebut ke Jenkins, lalu memberikan respon kembali ke klien. Hal ini diperlukan mod_proxy pada Nginx seperti berikut ini:

Ini mengasumsikan bahwa Anda menjalankan Jenkins pada port 8080.

```
upstream jenkins {
  keepalive 32;
  server 127.0.0.1:8080;
}
 
server {
  listen          80;
  server_name     jenkins.tuxtor.com;
  root            /var/run/jenkins/war/;
  access_log      /var/log/nginx/jenkins-access.log;
  error_log       /var/log/nginx/jenkins-error.log;
  ignore_invalid_headers off;

  location ~ "^/static/[0-9a-fA-F]{8}\/(.*)$" {
    rewrite "^/static/[0-9a-fA-F]{8}\/(.*)" /$1 last;
  }

  location /userContent {
    root /var/lib/jenkins/;
    if (!-f $request_filename){
      rewrite (.*) /$1 last;
      break;
    }
    sendfile on;
  }

  location / {
      sendfile off;
      proxy_pass         http://jenkins;
      proxy_redirect     default;
      proxy_http_version 1.1;

      proxy_set_header   Host              $host;
      proxy_set_header   X-Real-IP         $remote_addr;
      proxy_set_header   X-Forwarded-For   $proxy_add_x_forwarded_for;
      proxy_set_header   X-Forwarded-Proto $scheme;
      proxy_max_temp_file_size 0;

      client_max_body_size       10m;
      client_body_buffer_size    128k;

      proxy_connect_timeout      90;
      proxy_send_timeout         90;
      proxy_read_timeout         90;
      proxy_buffering            off;
      proxy_request_buffering    off;
      proxy_set_header Connection "";
  }
}
```

Agar pengaturan ini berfungsi, path Jenkins harus sama antara Nginx dan Jenkins (yaitu, Anda tidak dapat menjalankan Jenkins di `http://localhost:8081/jenkins` dan atau `http://localhost:80/jenkins)`.