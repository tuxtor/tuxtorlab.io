---
title: Start/Stop dan Restart Jenkins service di Windows
date: 2020-03-29
tags: ["jenkins", "windows"]
categories: DevOps Tutorials
---

Berikut cara menjalankan service Jenkins di Windows serta restart dan mematikan service Jenkins dengan menggunakan CMD:

<!--more-->

Pertama buka CMD terlebih dahulu, lalu masuk ke direktori instalasi Jenkins dengan menggunakan command cd, lalu jalankan command berikut.

Command untuk menjalankan service Jenkins:
```
    jenkins.exe start
```

Command untuk me-restart service Jenkins:
```
    jenkins.exe restart
```

Command untuk mematikan service Jenkins:
```
   jenkins.exe stop
```