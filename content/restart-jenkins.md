---
title: Restart Jenkins
date: 2020-03-29
tags: ["jenkins"]
categories: DevOps Tutorials
---

Setelah menginstal beberapa plugin pada Jenkins. Jenkins perlu di-restart terlebih dahulu agar plugin tersebut dapat digunakan. Untuk me-restart Jenkins caranya sebagai berikut:

<!--more-->

```
    (jenkins_url)/safeRestart
```
Dengan safeRestart akan me-restart Jenkins setelah job yang sedang berjalan selesai terlebih dahulu. Job yang baru akan tetap dalam antrian untuk berjalan setelah restart selesai.

```
    (jenkins_url)/restart
```
Dengan restart akan memaksa me-restart Jenkins tanpa menunggu job yang sedang berjalan selesai terlebih dahulu.