---
title: Securing MongoDB Linux Ubuntu
date: 2020-07-20
tags: ["mongodb", "ubuntu"]
categories: Linux Admin Tutorials
---

Konfigurasi default MongoDB setelah instalasi tidak secure. Kita bisa create, read, write, alter dan delete data dan login tanpa menggunakan user dan password. Untuk itu berikut cara securing MongoDB.

<!--more-->

Pertama login ke MongoDB:

```
mongo
```

Setelah masuk shell MongoDB, create user dan password baru:

```
db.createUser({
  user: "USERNAME", 
  pwd: "PASSWORD", 
  roles: [
    {
      role: "readWrite",
      db: "DATABASENAME"
    }
  ]
});
```

Enable authentication dan bind spesific ip pada konfigurasi MongoDB:

```
vi /etc/mongod.conf

security:
  authorization: enabled

net:
  port: 27017
  bindIp: 192.168.0.123
```

Restart service MongoDB:

```
systemctl restart mongod
```
