---
title: Add User ke Group di Linux
date: 2020-07-13
tags: ["ubuntu", "centos"]
categories: Linux Admin Tutorials
---

## View All Group

Untuk melihat semua group yang sudah ada, caranya sebagai berikut:

```
getent group
```

<!--more-->

## Add User ke Group

Untuk menambahkan user yang sudah ada ke group, caranya sebagai berikut:

```
usermod -a -G namagroup namauser
```

Untuk menambahkan user ke beberapa group secara bersamaan, caranya sebagai berikut:

```
usermod -a -G namagroup1,namagroup2,namagroup3 namauser
```

## View Groups User

Untuk menampilkan user tersebut masuk ke member group apa saja, caranya sebagai berikut:

```
groups namauser
```

## Congratulations

Dengan mengikuti command line di atas, Anda dapat menampilkan dan menambahkan user ke group di Linux Centos maupun Ubuntu.