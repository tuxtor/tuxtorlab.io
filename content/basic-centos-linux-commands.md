---
title: Basic CentOS Linux Commands
date: 2020-07-11
tags: ["centos"]
categories: Linux Admin Tutorials
---

Sebelum mempelajari tools Administrator Linux CentOS, penting untuk mencatat filosofi di balik command line administrasi Linux.

<!--more-->

Linux dirancang berdasarkan pada filosofi Unix. Linux, pada dasarnya, tidak memiliki aplikasi tujuan tunggal yang besar untuk satu penggunaan khusus di banyak waktu. Sebaliknya, ada ratusan utilitas dasar yang bila digabungkan menawarkan power besar untuk menyelesaikan tugas besar dengan efisiensi.

Contohnya, jika administrator menginginkan daftar semua user saat ini pada suatu sistem, command line berikut ini dapat digunakan untuk mendapatkan daftar semua user sistem. Command berikut akan menampilkan user pada sistem dalam urutan abjad.

```
[tuxtor@centos ~]$ cut /etc/passwd -d":" -f1 | sort
abrt
adm
bin
chrony
daemon
dbus
ftp
games
halt
libstoragemgmt
lp
mail
nobody
ntp
nxautomation
omi
omsagent
operator
polkitd
postfix
root
rpc
shutdown
sshd
sync
systemd-network
tcpdump
```

Mudah untuk mengekspor daftar tersebut ke file teks menggunakan command berikut.

```
[tuxtor@centos ~]$ cut /etc/passwd -d ":" -f1 > users.txt
```

Basic command line yang harus dikuasai setiap Administrator Linux:
- vim
- grep
- more and less
- tail
- head
- wc
- sort
- uniq
- tee
- cat
- cut
- sed
- tr
- paste
