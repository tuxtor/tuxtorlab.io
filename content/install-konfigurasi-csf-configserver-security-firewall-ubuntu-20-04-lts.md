---
title: Install dan konfigurasi CSF (ConfigServer Security & Firewall) di Ubuntu 20.04 LTS
date: 2020-07-19
tags: ["firewall", "ubuntu"]
categories: Linux Admin Tutorials
---

ConfigServer Security & Firewall (CSF) merupakan security tool untuk Linux. CSF menyediakan interface simple iptables untuk keamanan Linux server. Feature CSF diantaranya:

- Stateful Packet Inspection Firewall (SPI);
- Intrusion Detection;
- Login Failure Daemon;
- DDOS Protection;
- Control Panel Integration.

<!--more-->

## Install CSF

Update system terlebih dahulu:

```
apt update
apt upgrade
```

Remove UFW firewall:

```
apt remove ufw
```

Install dependencies CSF:

```
apt install perl zip unzip libwww-perl liblwp-protocol-https-perl sendmail-bin
```

Install CSF ke direktori **/opt**:

```
cd /opt
wget https://download.configserver.com/csf.tgz
tar -xzf csf.tgz
cd csf
sh install.sh
```

Verifikasi require iptables modules CSF:

```
perl /opt/csf/bin/csftest.pl
```

Check version CSF setelah berhasil diinstall:

```
csf -v
```

## Konfigurasi CSF

Defaultnya CSF jalan dengan mode **testing**, untuk itu disable mode testing:

```
vi /etc/csf/csf.conf

TESTING = "0"
RESTRICT_SYSLOG = "3"
```

Allow traffic by port:

```
vi /etc/csf/csf.conf

# Allow incoming TCP ports
TCP_IN = 22,80,443"

# Allow outgoing TCP ports
TCP_OUT = 20,21,22,25,26,37,43,53,80,110,113,443,465,873"
```

Allow by IP:

```
vi /etc/csf/csf.deny

192.168.0.123
192.168.1.0/24
```

Deny by IP:

```
192.168.2.0/24
```

Restart CSF agar perubahan diterapkan:

```
csf -ra
```
